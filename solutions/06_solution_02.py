from nilearn import input_data

masker = input_data.NiftiMapsMasker(
    msdl_data.maps, t_r=2, detrend=True,
    low_pass=.1, high_pass=.01
).fit()
