
mask_names = ['mask_vt', 'mask_face', 'mask_house']

mask_scores = {}
mask_chance_scores = {}

for mask_name in mask_names:
    print("Working on %s" % mask_name)
    mask_scores[mask_name] = {}
    mask_chance_scores[mask_name] = {}
    # Load the masker associated to the current mask `mask_name`
    mask_filename = haxby_dataset[mask_name][0]
    masker = NiftiMasker(mask_img=mask_filename, standardize=True)

    # For each category, fit and evaluate two models, one `svc_l1` and one `dummy_classifier`
    # and store their respective results in `mask_scores[mask_name][category]` and
    # `mask_chance_scores[mask_name][category]`
    for category in categories:
        print("Processing %s %s" % (mask_name, category))
        classification_target = (stimuli[task_mask] == category)
        # Specify the classifier to the decoder object.
        # With the decoder we can input the masker directly.
        # We are using the svc_l1 here because it is intra subject.
        decoder = Decoder(estimator='svc_l1', cv=cv,
                          mask=masker, scoring='roc_auc')
        decoder.fit(task_data, classification_target, groups=session_labels)
        mask_scores[mask_name][category] = decoder.cv_scores_[1]
        print("Scores: %1.2f +- %1.2f" % (
              np.mean(mask_scores[mask_name][category]),
              np.std(mask_scores[mask_name][category])))

        dummy_classifier = Decoder(estimator='dummy_classifier', cv=cv,
                                   mask=masker, scoring='roc_auc')
        dummy_classifier.fit(task_data, classification_target,
                             groups=session_labels)
        mask_chance_scores[mask_name][category] = dummy_classifier.cv_scores_[1]
