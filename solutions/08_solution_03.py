
category = 'face'

# Make a data splitting object for cross validation 
from sklearn.model_selection import LeaveOneGroupOut
cv = LeaveOneGroupOut()

# Select the classification target from `stimuli` that matches the category
classification_target = (stimuli[task_mask] == category)

# Create the decoder object with estimator `svc_l1`
from nilearn.decoding import Decoder
decoder = Decoder(estimator='svc_l1', cv=cv,
                  mask=masker, scoring='roc_auc')

# Call fit and passes `groups` to handle the cross validation.
decoder.fit(task_data, classification_target, groups=session_labels)
    
# Recover score for positive class
scores = decoder.cv_scores_[1]

# Display scores
print(f"Scores: {np.mean(scores):.2%} +- {np.std(scores):.2%}")
