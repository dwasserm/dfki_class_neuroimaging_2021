# apply the task_mask to  fMRI data (func_filename)
from nilearn.image import index_img
task_data = index_img(func_filename, task_mask)