# Print basic information on the dataset

print('First subject anatomical nifti image (3D) '
      f'located is at: {haxby_dataset.anat[0]}')
print('First subject functional nifti image (4D) '
      f'is located at: {haxby_dataset.func[0]}')