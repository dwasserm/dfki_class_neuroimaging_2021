from nilearn import plotting
from matplotlib import pyplot as plt
from nilearn.connectome import ConnectivityMeasure

correlation_measure = ConnectivityMeasure(kind='correlation')
correlation_matrices = correlation_measure.fit_transform(children)
mean_correlation_matrix = correlation_measure.mean_

print('correlation_matrices has shape {0}.'.format(correlation_matrices.shape))

# Make a single figure with 3 subplots
_, axes = plt.subplots(1, 3, figsize=(15, 5))
for i, (matrix, ax) in enumerate(zip(correlation_matrices, axes)):
    plotting.plot_matrix(matrix, tri='lower', colorbar=False, axes=ax,
                         title='correlation, child {}'.format(i))
    
plotting.plot_connectome(mean_correlation_matrix, msdl_coords,
                         title='mean correlation over all children')
