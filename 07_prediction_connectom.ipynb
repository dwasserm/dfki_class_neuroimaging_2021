{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# Decoding with FREM: face vs house object recognition\n",
    "\n",
    "In this notebook, we will see how to perform decoding using `sklearn` and\n",
    "`nilearn`. The goal of the task is to predict whether a subject is looking\n",
    "at a face vs house using the discrimination task from Haxby 2001 study.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the Haxby dataset\n",
    "\n",
    "For this task, we will first load the `Haxby 2001` dataset using `nilearn` helpers:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from nilearn.datasets import fetch_haxby\n",
    "data_files = fetch_haxby()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we load the behavioral data for the first subject and select the two conditions we are\n",
    "interested in:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Load behavioral data\n",
    "import pandas as pd\n",
    "behavioral = pd.read_csv(data_files.session_target[0], sep=\" \")\n",
    "\n",
    "# Restrict to face and house conditions\n",
    "conditions = behavioral['labels']\n",
    "condition_mask = conditions.isin(['face', 'house'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To be able to evaluate the model, we split the data between training and\n",
    "testing data using the `chunks`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Split data into train and test samples, using the chunks\n",
    "condition_mask_train = (condition_mask) & (behavioral['chunks'] <= 6)\n",
    "condition_mask_test = (condition_mask) & (behavioral['chunks'] > 6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Apply this sample mask to X (fMRI data) and y (behavioral labels)  \n",
    "Because the data is in one single large 4D image, we need to use\n",
    "`index_img` to do the split easily"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from nilearn.image import index_img\n",
    "func_filenames = data_files.func[0]\n",
    "X_train = index_img(func_filenames, condition_mask_train)\n",
    "X_test = index_img(func_filenames, condition_mask_test)\n",
    "y_train = conditions[condition_mask_train].values\n",
    "y_test = conditions[condition_mask_test].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the mean epi to be used for the background of the plotting\n",
    "from nilearn.image import mean_img\n",
    "background_img = mean_img(func_filenames)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using `scikit-learn` to decode functional imaging data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LogisticRegression\n",
    "decoder = LogisticRegression()\n",
    "decoder.fit(X_train, y_train)\n",
    "decoder.score(X_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The issue here is that the input data is not in the expected shape for `scikit-learn`.  \n",
    "It should be `n_samples, n_features` but it is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can thus convert this manually using `get_fdata` to get a numpy array and then using `reshape` and `transpose`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train_np = X_train.get_fdata().reshape(-1, X_train.shape[-1]).T\n",
    "X_test_np = X_test.get_fdata().reshape(-1, X_test.shape[-1]).T\n",
    "\n",
    "print(X_train_np.shape, X_test_np.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LogisticRegression\n",
    "logreg = LogisticRegression(C=1000)\n",
    "logreg.fit(X_train_np, y_train)\n",
    "accuracy = logreg.score(X_test_np, y_test)\n",
    "\n",
    "print(f\"Sklearn logreg classification accuracy : {accuracy:.2%}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using `nilearn.Decoder` for the same task:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To simplify this analysis, `nilearn` provides a pipeline that allows directly feeding `Nifti1Image`: the `nilearn.decoding.Decoder`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from nilearn.decoding import Decoder\n",
    "\n",
    "decoder = Decoder(estimator='logistic_l2', cv=10, scoring='balanced_accuracy', n_jobs=4)\n",
    "decoder.fit(X_train, y_train)\n",
    "accuracy_logreg = decoder.score(X_test, y_test)\n",
    "print(f\"Nilearn logreg classification accuracy: {accuracy_logreg:.2%}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from nilearn import plotting\n",
    "plotting.plot_stat_map(decoder.coef_img_[\"face\"], background_img,\n",
    "                       title=f\"Logreg: accuracy {accuracy_logreg:.2%}, 'face coefs'\",\n",
    "                       cut_coords=(-52, -5), display_mode=\"yz\")\n",
    "plotting.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The boost in performance here also comes from the averaging of several estimators to figth overfitting."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fit FREM\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fast Regularized Ensemble of Models (FREM) uses an\n",
    "implicit spatial regularization through fast clustering and aggregates a\n",
    "high number of estimators trained on various splits of the training set,\n",
    "thus returning a very robust decoder at a lower computational cost than\n",
    "other spatially regularized methods.\n",
    "\n",
    "To have more details, see: [`frem`](https://nilearn.github.io/modules/generated/nilearn.decoding.FREMClassifier.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from nilearn.decoding import FREMClassifier\n",
    "decoder = FREMClassifier(estimator='logistic_l2', cv=10, n_jobs=4)\n",
    "\n",
    "# Fit model on train data and predict on test data\n",
    "decoder.fit(X_train, y_train)\n",
    "accuracy_frem = (decoder.predict(X_test) == y_test).mean()\n",
    "print(f\"FREM classification accuracy : {accuracy_frem:.2%}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from nilearn import plotting\n",
    "plotting.plot_stat_map(decoder.coef_img_[\"face\"], background_img,\n",
    "                       title=\"FREM: accuracy %g%%, 'face coefs'\" % accuracy,\n",
    "                       cut_coords=(-52, -5), display_mode=\"yz\")\n",
    "plotting.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, ensembling procedure yields an important improvement of decoding\n",
    "accuracy on this simple example compared to fitting only one model per\n",
    "fold and the clustering mechanism keeps its computational cost reasonable\n",
    "even on heavier examples. Here we ensembled several instances of `logreg-l2`,\n",
    "but `FREMClassifier` also works with ridge or SVM.\n",
    "`FREMRegressor` object is also available to solve regression problems.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
