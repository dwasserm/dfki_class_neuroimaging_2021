{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# ROI-based decoding analysis in Haxby et al. dataset\n",
    "\n",
    "In this exercice, we reproduce the data analysis conducted by\n",
    "*Haxby et al.* in [\"Distributed and Overlapping Representations of Faces and\n",
    "Objects in Ventral Temporal Cortex\"](https://www.zora.uzh.ch/id/eprint/3224/2/haxby_science2001V.pdf).\n",
    "\n",
    "Specifically, we look at decoding accuracy for different objects in\n",
    "three different masks: the full ventral stream (`mask_vt`), the house\n",
    "selective areas (`mask_house`) and the face selective areas (`mask_face`),\n",
    "that have been defined via a standard GLM-based analysis.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load and prepare the data\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As in the previous example, we will work on the `Haxby` dataset provided and formated using `nilearn`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# Fetch data using nilearn dataset fetcher\n",
    "from nilearn import datasets\n",
    "# by default we fetch 2nd subject data for analysis\n",
    "haxby_dataset = datasets.fetch_haxby()\n",
    "func_filename = haxby_dataset.func[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's re-explore the `haxby_dataset` object:\n",
    "\n",
    "* Where is located the first subject anatomical nifti image?\n",
    "* Where is located the first subject functional nifti image?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load solutions/08_solution_01.py\n",
    "# Print basic information on the dataset\n",
    "print('First subject anatomical nifti image (3D) '\n",
    "      f'located is at: {...}')\n",
    "print('First subject functional nifti image (4D) '\n",
    "      f'is located at: {...}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# load labels\n",
    "import pandas as pd\n",
    "labels = pd.read_csv(haxby_dataset.session_target[0], sep=\" \")\n",
    "stimuli = labels['labels']\n",
    "# identify resting state labels in order to be able to remove them\n",
    "task_mask = (stimuli != 'rest')\n",
    "\n",
    "# find names of remaining active labels\n",
    "categories = stimuli[task_mask].unique()\n",
    "\n",
    "# extract tags indicating to which acquisition run a tag belongs\n",
    "session_labels = labels[\"chunks\"][task_mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, apply the `task_mask` to the functional data to obtain the `task_data`.\n",
    "Remember that you can use the `nilearn.image.index_img` helper."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# %load solutions/08_solution_02.py\n",
    "task_data = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Decoding faces with one mask"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load the `mask_face_little` and perform decoding using a `svc_l1` estimator.\n",
    "Note that:\n",
    "* Scoring should be done with `roc_auc`,\n",
    "* `cv` should be based on groups defined from `session_label`. This will make sure that recordings from the same session are not used in both train and test."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from nilearn.decoding import Decoder\n",
    "from nilearn.input_data import NiftiMasker\n",
    "\n",
    "mask_name = 'mask_face_little'\n",
    "mask_filename = haxby_dataset[mask_name][0]\n",
    "masker = NiftiMasker(mask_img=mask_filename, standardize=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load solutions/08_solution_03.py\n",
    "\n",
    "category = 'face'\n",
    "\n",
    "# Make a data splitting object for cross validation \n",
    "from sklearn.model_selection import \n",
    "cv = ...\n",
    "\n",
    "# Select the classification target from `stimuli` that matches the category\n",
    "classification_target = ...\n",
    "\n",
    "# Create the decoder object with estimator `svc_l1`\n",
    "from nilearn.decoding import Decoder\n",
    "decoder = Decoder(...)\n",
    "\n",
    "# Call fit and passes `groups` to handle the cross validation.\n",
    "decoder.fit(...)\n",
    "    \n",
    "# Recover score for positive class\n",
    "scores = decoder.cv_scores_[1]\n",
    "\n",
    "# Display scores\n",
    "print(f\"Scores: {np.mean(scores):.2%} +- {np.std(scores):.2%}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Decoding on the different masks\n",
    "\n",
    "The classifier used here is a support vector classifier (svc). We use\n",
    "class:`nilearn.decoding.Decoder` and specify the classifier.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use `nilearn.decoding.Decoder` to estimate a baseline.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# %load solutions/08_solution_04.py\n",
    "\n",
    "mask_names = ['mask_vt', 'mask_face', 'mask_house']\n",
    "\n",
    "mask_scores = {}\n",
    "mask_chance_scores = {}\n",
    "\n",
    "for mask_name in mask_names:\n",
    "    print(\"Working on %s\" % mask_name)\n",
    "    mask_scores[mask_name] = {}\n",
    "    mask_chance_scores[mask_name] = {}\n",
    "    # Load the masker associated to the current mask `mask_name`\n",
    "    masker = ...\n",
    "\n",
    "    # For each category, fit and evaluate two models, one `svc_l1` and one `dummy_classifier`\n",
    "    # and store their respective results in `mask_scores[mask_name][category]` and\n",
    "    # `mask_chance_scores[mask_name][category]`\n",
    "    ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## We make a simple bar plot to summarize the results\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from nilearn.plotting import show\n",
    "\n",
    "plt.figure()\n",
    "\n",
    "tick_position = np.arange(len(categories))\n",
    "plt.xticks(tick_position, categories, rotation=45)\n",
    "\n",
    "for color, mask_name in zip('rgb', mask_names):\n",
    "    score_means = [np.mean(mask_scores[mask_name][category])\n",
    "                   for category in categories]\n",
    "    plt.bar(tick_position, score_means, label=mask_name,\n",
    "            width=.25, color=color)\n",
    "\n",
    "    score_chance = [np.mean(mask_chance_scores[mask_name][category])\n",
    "                    for category in categories]\n",
    "    plt.bar(tick_position, score_chance,\n",
    "            width=.25, edgecolor='k', facecolor='none')\n",
    "\n",
    "    tick_position = tick_position + .2\n",
    "\n",
    "plt.ylabel('Classification accurancy (AUC score)')\n",
    "plt.xlabel('Visual stimuli category')\n",
    "plt.ylim(0.3, 1)\n",
    "plt.legend(loc='lower right')\n",
    "plt.title('Category-specific classification accuracy for different masks')\n",
    "plt.tight_layout()\n",
    "\n",
    "\n",
    "show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
