# First Inria-DFKI European School on Artificial Intelligence
# Track B - AI for Medicine
# Course: Introduction to Neuroimaging with Python

Notebooks for the class.

Requirement installation instructions can be found at [the NiLearn installation webpage](https://nilearn.github.io/introduction.html#installing-nilearn)
