{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# Classification of age groups using functional connectivity\n",
    "\n",
    "This example compares different kinds of functional connectivity between\n",
    "regions of interest : correlation, partial correlation, and tangent space\n",
    "embedding.\n",
    "\n",
    "The resulting connectivity coefficients can be used to\n",
    "discriminate children from adults. In general, the tangent space embedding\n",
    "**outperforms** the standard correlations: see `Dadi et al 2019\n",
    "<https://www.sciencedirect.com/science/article/pii/S1053811919301594>`_\n",
    "for a careful study.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load brain development fMRI dataset and MSDL atlas\n",
    "We study only 30 subjects from the dataset, to save computation time.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from nilearn import datasets\n",
    "\n",
    "development_dataset = datasets.fetch_development_fmri(n_subjects=30)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use probabilistic regions of interest (ROIs) from the MSDL atlas.  \n",
    "Load the atlas using `fetch_atlas_msdl` and see how many regions it has and which networks it covers.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# %load solutions/06_solution_01.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Region signals extraction\n",
    "To extract regions time series, instantiate a\n",
    "`nilearn.input_data.NiftiMapsMasker` object and pass the atlas\n",
    "file name to it.  \n",
    "In order to get clean signals, also use band\n",
    "pass filter with parameters [0.01, 0.1] and detrending option.\n",
    "\n",
    "*Notes:* The repetition time for the dataset is `t_r=2`.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# %load solutions/06_solution_02.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, compute region signals and extract useful phenotypic informations for the processing.\n",
    "Create three list such that:\n",
    "\n",
    "- List `subjects` contains the time_series extracted from `development_dataset.func`\n",
    "  using the `masker` object created above the all the confounds associated with this subject.\n",
    "- List `groups` contains the target group associated with the matching subject.\n",
    "- List `children` contains the processed time_series for all the children."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# %load solutions/06_solution_03.py\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ROI-to-ROI correlations of children\n",
    "\n",
    "Use `nilearn.connectome.ConnectivityMeasure` to compute the conectom for each children\n",
    "using the `correlation`.  \n",
    "What is the shape of the object returned by `ConnectivityMeasure.transform`?\n",
    "\n",
    "Then, visualize the correlation matrix for the 3 first children using\n",
    "`plot_matrix` and then display the average connectom using `plot_connectome` function.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# %load solutions/06_solution_04.py\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Studying partial correlations\n",
    "\n",
    "Now, repeat this study for **direct connections**, revealed by partial correlation\n",
    "coefficients.\n",
    "\n",
    "*Note that most of direct connections are weaker than full connections.*\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# %load solutions/06_solution_05.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extract subjects variabilities around a group connectivity\n",
    "\n",
    "Now, we can use **both** correlations and partial correlations to capture\n",
    "reproducible connectivity patterns at the group-level.\n",
    "This is done by the tangent space embedding.\n",
    "\n",
    "Repeat the previous analysis for the `tangent` space embedding.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# %load solutions/06_solution_06.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now plot the connectome using the mean of the `tangent_matrices` over all children.  \n",
    "Is this equivalent to the connectom obtained with `tangeant_measure.mean_`?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load solutions/06_solution_07.py\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Note:* `tangent_matrices` model individual connectivities as\n",
    "**perturbations** of the group connectivity matrix `tangent_measure.mean_`.\n",
    "Keep in mind that these subjects-to-group variability matrices do not\n",
    "directly reflect individual brain connections. For instance negative\n",
    "coefficients can not be interpreted as anticorrelated regions.  \n",
    "The average tangent matrix cannot be interpreted, as individual matrices\n",
    "represent deviations from the mean, which is set to 0.  \n",
    "When we fit our children group, we get the group connectivity matrix stored as\n",
    "in `tangent_measure.mean_`, and individual deviation matrices of each subject\n",
    "from it.\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What kind of connectivity is most powerful for classification?\n",
    "\n",
    "We will now use connectivity matrices as features to distinguish children from\n",
    "adults. We use cross-validation and measure classification accuracy to\n",
    "compare the different kinds of connectivity matrices.\n",
    "We use random splits of the subjects into training/testing sets.\n",
    "StratifiedShuffleSplit allows preserving the proportion of children in the\n",
    "test set.\n",
    "\n",
    "For each measure of connectivity (`'correlation', 'partial correlation', 'tangent'`),\n",
    "compute the connectom of the training and test set and evaluate the accuracy of\n",
    "a `sklearn.svm.LinearSVD` model for the task.\n",
    "\n",
    "All results should be stored in a `scores` dictionary, such that each kind of\n",
    "connectivity measure is associated to the list of cv scores.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# %load solutions/06_solution_08.py\n",
    "from sklearn.svm import LinearSVC\n",
    "from sklearn.model_selection import StratifiedShuffleSplit\n",
    "from sklearn.metrics import accuracy_score\n",
    "import numpy as np\n",
    "\n",
    "kinds = ['correlation', 'partial correlation', 'tangent']\n",
    "cv = StratifiedShuffleSplit(n_splits=15, random_state=0, test_size=5)\n",
    "\n",
    "scores = {}\n",
    "for kind in kinds:\n",
    "    scores[kind] = []\n",
    "    # for each split by cv, append the accuracy obtained on the\n",
    "    # test set with a `LinearSVC` model.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This small utils will no display the results:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "mean_scores = [np.mean(scores[kind]) for kind in kinds]\n",
    "scores_std = [np.std(scores[kind]) for kind in kinds]\n",
    "\n",
    "plt.figure(figsize=(6, 4))\n",
    "positions = np.arange(len(kinds)) * .1 + .1\n",
    "plt.barh(positions, mean_scores, align='center', height=.05, xerr=scores_std)\n",
    "yticks = [k.replace(' ', '\\n') for k in kinds]\n",
    "plt.yticks(positions, yticks)\n",
    "plt.gca().grid(True)\n",
    "plt.gca().set_axisbelow(True)\n",
    "plt.gca().axvline(.8, color='red', linestyle='--')\n",
    "plt.xlabel('Classification accuracy\\n(red line = chance level)')\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a small example to showcase nilearn features. In practice such\n",
    "comparisons need to be performed on much larger cohorts and several\n",
    "datasets.\n",
    "`Dadi et al 2019\n",
    "<https://www.sciencedirect.com/science/article/pii/S1053811919301594>`_\n",
    "Showed that across many cohorts and clinical questions, the tangent\n",
    "kind should be preferred.\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
